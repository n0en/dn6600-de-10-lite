module RomInitBinary
	(
		input 								clock,
		input [HIGH_ADDRESS_BIT:0]		address,
		output reg [HIGH_DATA_BIT:0]	outputData
	);
	
	parameter DATA_WIDTH = 18 ;
	parameter HIGH_DATA_BIT = DATA_WIDTH - 1;
	parameter ADDR_WIDTH = 8 ;
	parameter HIGH_ADDRESS_BIT = ADDR_WIDTH - 1;
	parameter RAM_DEPTH = 1 << ADDR_WIDTH;
	parameter MAX_ADDRESS = RAM_DEPTH - 1;
	parameter INIT_FILE = "dn355_ROM.init";
	
	reg [HIGH_DATA_BIT:0] rom [0:MAX_ADDRESS];
	
	initial
	begin
		$readmemb(INIT_FILE, rom);
	end
	
	always @ (posedge(clock))
		begin
			outputData <= rom[address];
		end
		
endmodule
