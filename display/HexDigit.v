module HexDigit (
	input wire [3:0] inputValue,
	output wire [7:0] displayDigit
);

   function automatic [6:0] digit;
      input [3:0] num; 
      case (num)
         0:  digit = 7'b1000000;  // 0
         1:  digit = 7'b1111001;  // 1
         2:  digit = 7'b0100100;  // 2
         3:  digit = 7'b0110000;  // 3
         4:  digit = 7'b0011001;  // 4
         5:  digit = 7'b0010010;  // 5
         6:  digit = 7'b0000010;  // 6
         7:  digit = 7'b1111000;  // 7
			8:  digit = 7'b0000000;  // 8
			9:  digit = 7'b0010000;  // 9
			10:  digit = 7'b0001000;  // A
			11:  digit = 7'b0000011;  // b
			12:  digit = 7'b1000110;  // C
			13:  digit = 7'b0100001;  // d
			14:  digit = 7'b0000110;  // E
			15:  digit = 7'b0001110;  // F
      endcase
   endfunction

	assign displayDigit[6:0] = digit(inputValue);
	assign displayDigit[7] = 1;

endmodule
