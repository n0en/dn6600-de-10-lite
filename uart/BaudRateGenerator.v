// Baud Rate Generator

`include "../common.h"

module BaudRateGenerator(
	input wire clk,
	input wire resetq,
	input wire [31:0] baud,
	input wire restart,
	output wire ser_clk,
	output reg [23:0] testpoint
);

	wire [38:0] aclkfreq = `CLK_FREQ_HZ;
	reg [38:0] d;
	wire [38:0] dInc = d[38] ? ({4'd0, baud}) : (({4'd0, baud}) - aclkfreq);
	wire [38:0] dN = restart ? 0 : (d + dInc);
	wire fastclk = ~d[38];
	assign ser_clk = fastclk;

	always @(negedge resetq or posedge clk)
	begin
		testpoint <= d;
		if (!resetq) begin
			d <= 0;
		end else begin
			d <= dN;
		end
	end
	
endmodule
