module BitTransmitter(
	input wire fclk,				// Fast system clock
	input wire bclk,				// 10 x baud rate clock
	input wire write,				// Data to transmit is latched on rising edge
	input wire reset,				// Stops transmission and clears registers
	input wire [7:0] tx_data,	// Byte to transmit
	
	output reg busy,				// Indicates when holding register is full
	output reg tx					// Transmitter output
);

	localparam STATE_IDLE					= 3'b000;
	localparam STATE_START_TX				= 3'b001;
	localparam STATE_SENDING_START_BIT 	= 3'b010;
	localparam STATE_SENDING_DATA_BIT	= 3'b011;
	localparam STATE_SENDING_STOP_BIT	= 3'b100;

	reg [7:0] holding_reg;	// Holds next byte to be trasmitted
	reg holding_ready;		// Indicates there is a byte to be transmitted in holding_reg
	reg [7:0] tx_reg;			// Holds byte currently being transmitted
	reg [3:0] tx_count;		// Number of bclk cycles left in current bit
	reg [3:0] send_count;	// Number of bits left to send in tx_reg
	reg last_bclk;				// Last sampled value of bclk (for edge detection)
	reg bclk_rising_edge;	// Flag indicating that the bclk had a rising edge on this cycle
	reg last_write;			// Last sampled value of write (for edge detection)
	reg write_rising_edge;	// Flag indicating that the write signa had a rising edge on this cycle
	
	reg [2:0] current_state;
	
	
	always @ (posedge fclk or posedge reset)
	begin
		if (reset)
		begin
			holding_reg <= 8'h00;
			holding_ready <= 1'b0;
			tx_reg <=8'b0;
			tx_count <= 4'b0;
			tx <= 1'b1;
			busy <= 1'b0;
			send_count <= 4'b0;
			last_bclk <= bclk;
			last_write <= write;
			bclk_rising_edge <= 1'b0;
			write_rising_edge <= 1'b0;
			current_state <= STATE_IDLE;
		end
		else
		begin
			bclk_rising_edge <= bclk & ~last_bclk;
			last_bclk <= bclk;
			write_rising_edge <= write & ~last_write;
			last_write <= write;
			if (write_rising_edge & ~busy)
			begin
				holding_reg <= tx_data;
				holding_ready <= 1'b1;
				busy <= 1'b1;
			end
			
			case (current_state)
			
				// Nothing to send
				STATE_IDLE:
				begin
					tx <= 1'b1;
					if (holding_ready) current_state <= STATE_START_TX;
				end
				
				// Set up to start transmission
				STATE_START_TX:
				begin
					tx_reg <= holding_reg;
					holding_ready <= 1'b0;
					send_count <= 4'd8;
					busy <= 1'b0;
					current_state <= STATE_SENDING_START_BIT;
				end
				
				// Synchronize to bit clock and start sending the start bit
				STATE_SENDING_START_BIT:
				begin
					if (bclk_rising_edge)
					begin
						tx <= 1'b0;
						tx_count <= 4'd9;
						current_state <= STATE_SENDING_DATA_BIT;
					end
				end
				
				// Send data bits synchronized with the bit clock
				STATE_SENDING_DATA_BIT:
				begin
					if (bclk_rising_edge)
					begin
						if (tx_count == 0)
						begin
							// We finished sending a bit, decide if there are more or not
							if (send_count == 0)
							begin
								tx <= 1'b1;
								current_state <= STATE_SENDING_STOP_BIT;
							end
							else
							begin
								tx <= tx_reg[0];
								tx_reg <= tx_reg >> 1;
								send_count <= send_count - 4'b1;
							end
							tx_count <= 4'd9;
						end
						else
						begin
							tx_count <= tx_count - 4'b1;
						end
					end
				end
				
				// Send the stop bit
				STATE_SENDING_STOP_BIT:
				begin
					if (bclk_rising_edge)
					begin
						if (tx_count == 0)
						begin
							current_state <= STATE_IDLE;
						end
						else
						begin
							tx_count <= tx_count - 4'b1;
						end
					end
				end
			
			endcase
			
		end
	end
	
	

endmodule
