module CharEcho(
	input wire clk,         		// System clock
   input wire resetq,				// Reset to idle state
	input wire busy,					// High when UART is sending
	input wire valid, 				// High when UART has received a character
   input wire [7:0] rx_data,	 	// Received character data (must be readable when valid goes high)
	
	output reg[7:0] tx_data,		// Character to be sent when write is strobed high
	output reg read,					// Strobed high once rx_data has been read
	output reg write					// Strobed high when tx_data is ready to be sent
);

	reg [2:0] state;					// Current state
	
	localparam STATE_IDLE 				= 3'b000;
	localparam STATE_RX_CHAR 			= 3'b001;
	localparam STATE_TX_CHAR_LOAD 	= 3'b010;
	localparam STATE_TX_WRITE 			= 3'b011;
	localparam STATE_WAIT_BUSY			= 3'b100;
	localparam STATE_WAIT_NOT_BUSY	= 3'b101;
	
	reg[7:0] temp_char;

	always @ (posedge clk or negedge resetq)
	begin
		if (!resetq)
		begin
			state <= STATE_IDLE;
		end
		else
		begin
		
			case (state)
			
				STATE_IDLE:
				begin
					read = 1'b0;
					write = 1'b0;
					if (valid) state <= STATE_RX_CHAR;
				end
				
				STATE_RX_CHAR:
				begin
					temp_char <= rx_data;
					state <= STATE_TX_CHAR_LOAD;
				end
				
				STATE_TX_CHAR_LOAD:
				begin
					read <= 1'b1;
					tx_data <= temp_char;
					state <= STATE_TX_WRITE;
				end
				
				STATE_TX_WRITE:
				begin
					read <= 1'b0;
					write <= 1'b1;
				end
				
				STATE_WAIT_BUSY:
				begin
					write <= 1'b0;
					if (busy) state <= STATE_WAIT_NOT_BUSY;
				end
				
				STATE_WAIT_NOT_BUSY:
				begin
					if (!busy) state <= STATE_IDLE;
				end
				
			endcase
		end
	end
	
endmodule