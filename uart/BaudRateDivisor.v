module BaudRateDivisor(
	input wire clk,
	input wire [2:0] baud_rate,	// 000 = 300, 001 = 1200, 010 = 4800, 011 = 9600
	output reg [23:0] divisor		// Calculated for a 50 MHz clock (actual sample rate which is 10 x baud rate)
);

	always @ (posedge clk)
		case (baud_rate)
			3'b000: divisor = 16667;
			3'b001: divisor = 4167;
			3'b010: divisor = 1042;
			3'b011: divisor = 521;
		endcase

endmodule
