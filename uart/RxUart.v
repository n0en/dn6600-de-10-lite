// Implement the receive side of a serial UART

module RxUart(
	input wire ser_clk,			// Baud rate serial clock
	input wire clk,				// System clock
   input wire resetq,
   input wire [31:0] baud,
   input wire uart_rx,      	// UART recv wire
   input wire rd,           	// read strobe
   output wire valid,       	// has data 
   output wire [7:0] data,	 	// data
	output ser_clk_restart		// Baud rate serial clock restart
);

	reg [4:0] bitcount;
	reg [7:0] shifter;

	// On starting edge, wait 3 half-bits then sample, and sample every 2 bits thereafter

	wire idle = &bitcount;
	wire sample;
	reg [2:0] hh = 3'b111;
	wire [2:0] hhN = {hh[1:0], uart_rx};
	wire startbit = idle & (hhN[2:1] == 2'b10);
	wire [7:0] shifterN = sample ? {hh[1], shifter[7:1]} : shifter;

	assign ser_clk_restart = startbit;
	
	assign valid = (bitcount == 18);
	reg [4:0] bitcountN;
	always @*
		if (startbit)
			bitcountN = 0;
		else if (!idle & !valid & ser_clk)
			bitcountN = bitcount + 5'd1;
		else if (valid & rd)
			bitcountN = 5'b11111;
		else
			bitcountN = bitcount;

	// 3,5,7,9,11,13,15,17
	assign sample = (bitcount > 2) & bitcount[0] & !valid & ser_clk;
	assign data = shifter;

	always @(negedge resetq or posedge clk)
	begin
		if (!resetq) begin
			hh <= 3'b111;
			bitcount <= 5'b11111;
			shifter <= 0;
		end else begin
			hh <= hhN;
			bitcount <= bitcountN;
			shifter <= shifterN;
		end
	end
	
endmodule
