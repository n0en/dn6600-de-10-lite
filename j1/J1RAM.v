module J1Ram(
        input wire        clk,

        input  wire[15:0] a_addr,
        output wire[31:0] a_q,
        input  wire[31:0] a_d,
        input  wire       a_wr,

        input  wire[12:0] b_addr,
        output wire[15:0] b_q);

        wire [31:0] insn32;

        DualPortBlockRam #(.DATA(32), .ADDR(12)) nram (
                .a_clk(clk),
                .a_wr(a_wr),
                .a_addr(a_addr[13:2]),
                .a_din(a_d),
                .a_dout(a_q),

                .b_clk(clk),
                .b_wr(1'b0),
                .b_addr(b_addr[12:1]),
                .b_din(32'd0),
                .b_dout(insn32));

        reg ba_;
        always @(posedge clk)
                ba_ <= b_addr[0];
        
        assign b_q = ba_ ? insn32[31:16] : insn32[15:0];

endmodule

